import { withPluginApi } from "discourse/lib/plugin-api";

export default {
  name: "discourse-placeholder-theme-component_lush",

  initialize() {
    withPluginApi("0.8.7", api => {
      api.decorateCooked(
        ($cooked, postWidget) => {
          if (!postWidget) return;

          const placeholderNodes = $cooked[0].querySelectorAll(
            ".d-lush[data-lush=placeholder]:not(.placeholdered)"
          );

          placeholderNodes.forEach(elem => {
            const dataKey = elem.dataset.key;

            if (!dataKey) return;

            const span = document.createElement("span");
            span.setAttribute("style", "color: #a40f20;");
            span.innerText = dataKey;

            // content has been set inside the [wrap][/wrap] block
            if (elem.querySelector("p")) {
              elem.querySelector("p").prepend(span);
            } else {
              elem.prepend(span);
            }

          });
        },
        { onlyStream: true, id: "discourse-placeholder-theme-component" }
      );
    });
  }
};
